"""
Run the project.
"""
import logging
import sys

import click


try:
    __import__('network_config')
except ImportError:
    sys.exit('Configuration file `network_config` cannot be found. Exiting.')


@click.group()
def commands():
    pass


@commands.command()
@click.option('--simulation')
@click.option('--logger', default='True')
@click.option('--repeats', default='1')
def cli(simulation, logger, repeats):
    import os

    from network.simulations import (Simulation, FailureSimulation,
                                     InvasionSimulation, FailureInvasionSimulation,
                                     AttachedSimulation, FailureAttachedSimulation,
                                     InvasionAttachedSimulation,
                                     FailureInvasionAttachedSimulation)

    simulation_classes = [Simulation, FailureSimulation,
                          InvasionSimulation, FailureInvasionSimulation,
                          AttachedSimulation, FailureAttachedSimulation,
                          InvasionAttachedSimulation, FailureInvasionAttachedSimulation]

    for simulation_class in simulation_classes:
        if simulation_class.__name__ == simulation:
            chosen_simulation_class = simulation_class
            break
    else:
        chosen_simulation_class = Simulation

    logger_ = logging.getLogger('network.simulations')
    if logger.capitalize() == 'True':
        logger_.level = logging.INFO

    try:
        iterations = int(repeats)
        if iterations < 1:
            raise ValueError
    except ValueError:
        raise ValueError(f'Parameter --repeats can only be set to positive integers, value `{repeats}` is invalid.')

    if 'results' not in os.listdir('.'):
        os.mkdir('results')

    class_name = chosen_simulation_class.__name__
    if class_name not in os.listdir('results'):
        os.mkdir(os.path.join('results', class_name))

    for iteration in range(1, iterations + 1):
        chosen_simulation = chosen_simulation_class()

        files_count = len(os.listdir(os.path.join('results', class_name)))
        result_file_name = os.path.join('results', class_name, f'results_{files_count + 1}.json')

        logger_.info(f'Running simulation {iteration}/{iterations}.')
        chosen_simulation.run(filename=result_file_name)


@commands.command()
@click.option('--simulation')
def gui(simulation):
    from gui import StepSimulationApp
    from gui import simulations as gui_simulations

    if simulation in gui_simulations.keys():
        chosen_simulation = simulation
    else:
        chosen_simulation = 'Simulation'

    app = StepSimulationApp(simulation=chosen_simulation)
    app.run()


if __name__ == '__main__':
    commands()
