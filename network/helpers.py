import math
from collections import namedtuple

from network import pywans_random

Movement = namedtuple('Movement', ('x', 'y'))


class Position:
    """
    Class representing position with 2 fields: x an y for respective dimension.
    It behaves just like a tuple (unpacking, setting, etc), but has possibility
    to add not as an extension but rather adding x to x and y to y elements.
    It supports both position.x and position[0] data access ways for objects
    for addition and equation of those.
    """
    __slots__ = ('x', 'y')

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'Position(x={self.x}, y={self.y})'

    def __eq__(self, other):
        try:
            return self.x == other.x and self.y == other.y
        except AttributeError:
            return self.x == other[0] and self.y == other[1]

    def __add__(self, position):
        try:
            return type(self)(
                self.x + position.x,
                self.y + position.y
            )
        except AttributeError:
            return type(self)(
                self.x + position[0],
                self.y + position[1]
            )

    def __iter__(self):
        yield self.x
        yield self.y


class Force(object):
    """
    Represents a force which is affecting given node with a load. Can be
    added with other forces as well as aggregated via sum class method.
    """
    __slots__ = ('x', 'y')

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        return f'Force(x={self.x}, y={self.y})'

    def __add__(self, force):
        return type(self)(
            self.x + force.x,
            self.y + force.y
        )

    def __sub__(self, force):
        return type(self)(
            self.x - force.x,
            self.y - force.y
        )

    def __eq__(self, force):
        return self.x == force.x and self.y == force.y

    def __neg__(self):
        return type(self)(x=-self.x, y=-self.y)

    @classmethod
    def sum(cls, *forces):
        return cls(
            x=sum(force.x for force in forces),
            y=sum(force.y for force in forces)
        )


def sign(number):
    """
    Works just like signum function- returns sign of given number.
    """
    return (number > 0) - (number < 0)


def get_distance(obj, position):
    """
    Returns distance between object and given position. The position
    needs to be able to unpack. The object needs to have position parameter
    with the same characteristics.
    """
    obj_x, obj_y = obj.position
    pos_x, pos_y = position

    distance_x = abs(obj_x - pos_x)
    distance_y = abs(obj_y - pos_y)

    return (distance_x ** 2 + distance_y ** 2) ** (1 / 2)


def get_movement(obj, position):
    """
    Calculates and returns tuple with given movements on x and y axis.
    It's calculated based on distance between object and position
    (just like get_distance) and object's speed attribute.
    """
    obj_x, obj_y = obj.position
    pos_x, pos_y = position

    distance_x = pos_x - obj_x
    distance_y = pos_y - obj_y
    distance = get_distance(obj, position)

    if distance > obj.speed:
        movement_x = obj.speed * distance_x / distance if distance != 0 else 0
        movement_y = obj.speed * distance_y / distance if distance != 0 else 0
    else:
        movement_x = distance_x
        movement_y = distance_y

    movement = Movement(movement_x, movement_y)
    return movement


def move_by_force(obj, force, map_size):
    """
    Moves given object equivalently to the force that is used on it.
    If force would cause the movement to quit the map space, object is instead
    moved as near to the wall as possible.
    """
    movement_x = math.atan(force.x) / 2 * obj.speed
    movement_y = math.atan(force.y) / 2 * obj.speed
    movement = Movement(movement_x, movement_y)

    if abs(movement.x) + abs(movement.y) < obj.speed / 20:
        return Movement(0, 0)

    # new position's x and y are to be between 1 and map_size - 1
    new_position = (
        max(min(obj.position.x + movement.x, map_size - 1), 1),
        max(min(obj.position.y + movement.y, map_size - 1), 1),
    )

    obj.position.x, obj.position.y = new_position

    return movement


def random_position(minimum=0, maximum=100):
    """
    Returns position tuple of random float values between given minimum
    and maximum of the default ones.
    """
    return (
        pywans_random.uniform(minimum, maximum),
        pywans_random.uniform(minimum, maximum),
    )


def random_sidemap_position(minimum=0, maximum=100):
    """
    Returns position tuple of random float values above/below given
    minimum and maximum. (Positions at the verges of the environment.
    """
    practical_minimum = max(minimum + 1, 0)
    practical_maximum = max(maximum - 1, practical_minimum)

    if pywans_random.randint(0, 1):
        if pywans_random.randint(0, 1):
            first = pywans_random.uniform(minimum, practical_minimum)
            second = pywans_random.uniform(minimum, maximum)
        else:
            first = pywans_random.uniform(practical_maximum, maximum)
            second = pywans_random.uniform(minimum, maximum)
    else:
        if pywans_random.randint(0, 1):
            first = pywans_random.uniform(minimum, maximum)
            second = pywans_random.uniform(minimum, practical_minimum)
        else:
            first = pywans_random.uniform(minimum, maximum)
            second = pywans_random.uniform(practical_maximum, maximum)

    return first, second
