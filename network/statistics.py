class MovementStatistics:
    __slots__ = ('movements',)

    def __init__(self):
        self.movements = []

    @property
    def movements_distance(self):
        return sum(abs(movement.x) + abs(movement.y) for movement in self.movements)


class MovementStatisticsMixin:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.movement_statistics = MovementStatistics()

    def add_movement(self, movement):
        self.movement_statistics.movements.append(movement)

    @property
    def distance_traveled(self):
        return self.movement_statistics.movements_distance
