import network_config as config


class BaseField:
    types = tuple()

    def __init__(self, field_name):
        self._field_name = field_name

        if hasattr(config, field_name):
            value = getattr(config, field_name)
        else:
            raise ValueError(f'{field_name} does not exist in the configuration file.')

        if self.validate(value):
            self.other_rules(field_name, value)
            self._value = value
        else:
            raise ValueError(f'Expected value for attribute {self._field_name} '
                             f'to be one of types: {self.types}. Instead got type'
                             f'{type(value)}.')

    def other_rules(self, field_name, value):
        pass

    def __get__(self, instance, owner):
        return self._value

    def __add__(self, other):
        return self._value + other

    def __mul__(self, other):
        return self._value * other

    def __radd__(self, other):
        return self.__add__(other)

    def __gt__(self, other):
        return self._value > other

    def __lt__(self, other):
        return self._value < other

    def __eq__(self, other):
        return self._value == other

    def __ne__(self, other):
        return self._value != other

    def __le__(self, other):
        return self._value <= other

    def __ge__(self, other):
        return self._value >= other

    def validate(self, value):
        return isinstance(value, self.types)


class PositiveFieldMixin:
    def other_rules(self, field_name, value):
        if value <= 0:
            raise ValueError(f'Value `{value}` for setting `{field_name}` expected to be positive.')

        next_ancestor = super()
        if hasattr(next_ancestor, 'other_rules'):
            next_ancestor.other_rules(field_name, value)


class NotNegativeFieldMixin:
    def other_rules(self, field_name, value):
        if value < 0:
            raise ValueError(f'Value `{value}` for setting `{field_name}` expected to be positive or equal to 0.')

        next_ancestor = super()
        if hasattr(next_ancestor, 'other_rules'):
            next_ancestor.other_rules(field_name, value)


class BooleanField(BaseField):
    types = (bool,)


class IntegerField(BaseField):
    types = (int,)


class NumberField(BaseField):
    types = (int, float)


class PositiveIntegerField(PositiveFieldMixin, IntegerField):
    pass


class NotNegativeIntegerField(NotNegativeFieldMixin, IntegerField):
    pass


class PositiveNumberField(PositiveFieldMixin, NumberField):
    pass
