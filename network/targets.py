"""
Target positioned on map, which is created as a primary threat for Nodes.
Targets are to be captured by Nodes in shortest possible time.
"""
from network.helpers import (Position, random_position,
                             get_distance, get_movement)
from network import fields


class Target:
    """Map object which is primary target for Nodes."""
    max_lifetime = fields.PositiveIntegerField('TARGET_MAX_LIFETIME')
    default_lifetime = fields.NotNegativeIntegerField('TARGET_START_LIFETIME')

    def __init__(self, position=None, is_active=True, pursued_by=None,
                 caught=False, name=None, max_lifetime=None):
        if position is None:
            self.position = Position(*random_position())
        else:
            self.position = Position(*position)

        self.lifetime = self.default_lifetime
        self.pursued_by = pursued_by
        self.caught = caught
        self.is_active = is_active
        self.name = name
        if max_lifetime is not None:
            self.max_lifetime = max_lifetime
        self.has_actions = False

    def age(self, value=1):
        self.lifetime += value

    def __repr__(self):
        return (
            f'Target(position={self.position}, lifetime={self.lifetime}, '
            f'caught={self.caught}, '
            f'pursued_by={self.pursued_by.name if self.pursued_by else None})'
        )

    @property
    def as_json(self):
        return {
            'name': self.name,
            'lifetime': self.lifetime,
            'max_lifetime': self.max_lifetime,
            'pursued_by': self.pursued_by.name if self.pursued_by is not None else 'None',
            'is_active': self.is_active,
            'caught': self.caught
        }


class MovingTarget(Target):
    """Target moving through map to it's centre."""
    speed = fields.PositiveNumberField('TARGET_SPEED')

    def __init__(self, *_, speed=None, **kwargs):
        super().__init__(**kwargs)
        if speed is not None:
            self.speed = speed
        self.has_actions = True

    def move_to_position(self, position):
        """
        If target position is within sight of the object, it is moved to the
        location. Otherwise movement is calculated based on object's speed,
        position and direction of the movement.
        """
        distance = get_distance(self, position)
        if distance <= self.speed:
            self.position = position
        else:
            self.position += get_movement(self, position)

    def perform_actions(self, simulation):
        map_centre = simulation.map_centre

        self.move_to_position(map_centre)

    def __repr__(self):
        return (
            f'MovingTarget(position={self.position}, lifetime={self.lifetime}, '
            f'caught={self.caught}, pursued_by={self.pursued_by}, speed={self.speed})'
        )

    @property
    def as_json(self):
        # Add to Target's json added field - speed.
        return dict(super().as_json, speed=self.speed)
