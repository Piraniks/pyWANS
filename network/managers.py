from network import pywans_random
from network.nodes import Node, AttachedNode
from network.targets import Target, MovingTarget


class Manager:
    model = None

    def __init__(self):
        self._objects = []
        self._active_objects = []

    @property
    def active(self):
        return self._active_objects

    @property
    def all(self):
        return self._objects

    @property
    def active_count(self):
        return len(self.active)

    @property
    def count(self):
        return len(self.all)

    @property
    def next_name(self):
        return f'{self.model.__name__}{self.count + 1}'

    def create(self, *args, **kwargs):
        new_object = self.model(*args, **kwargs, name=self.next_name)
        self._objects.append(new_object)
        if new_object.is_active:
            self._active_objects.append(new_object)

        return new_object


class NodeManager(Manager):
    model = Node

    @property
    def as_json(self):
        objects = [obj.as_json for obj in self.all]
        active_objects = [obj.as_json for obj in self.active]

        return {
            'model': self.model.__name__,
            'active_count': self.active_count,
            'average_distance_traveled': self.average_distance_traveled,
            'lowest_distance_traveled': self.lowest_distance_traveled,
            'highest_distance_traveled': self.highest_distance_traveled,
            'count': self.count,
            'objects': objects,
            'active_objects': active_objects
        }

    def deactivate(self, node):
        node.is_active = False
        try:
            self._active_objects.remove(node)
        except ValueError:
            pass

    def random(self, number=1, active=True):
        if active is True:
            maximum_nodes = self.active_count
            node_pool = self._active_objects
        else:
            maximum_nodes = self.count
            node_pool = self._objects

        if number > maximum_nodes:
            return self._objects

        if number == 1:
            return pywans_random.choice(node_pool)

        return pywans_random.sample(node_pool, number)

    def closest(self, position):
        nodes = {
            node: node.distance(position)
            for node in self.active
            if node.target is None and node.is_in_range(position)
        }

        sorted_nodes = sorted(nodes, key=nodes.get)

        return sorted_nodes

    def nodes_in_range(self, position, exclude=True):
        nodes = [node for node in self.active if node.is_in_range(position)]
        if exclude is True:
            for node in nodes:
                if node.position == position:
                    nodes.remove(node)

        return nodes

    @property
    def average_distance_traveled(self):
        distances = [obj.distance_traveled for obj in self.active]
        return sum(distances) / len(distances) if distances else 0

    @property
    def highest_distance_traveled(self):
        return max(obj.distance_traveled
                   for obj in self.active) if self.active else None

    @property
    def lowest_distance_traveled(self):
        return min(obj.distance_traveled
                   for obj in self.active) if self.active else None


class AttachedNodeManager(NodeManager):
    model = AttachedNode


class TargetManager(Manager):
    model = Target

    @property
    def as_json(self):
        objects = [obj.as_json for obj in self.all]
        active_objects = [obj.as_json for obj in self.active]

        return {
            'model': self.model.__name__,
            'active_count': self.active_count,
            'count': self.count,
            'average_lifetime': self.average_lifetime,
            'highest_lifetime': self.highest_lifetime,
            'lowest_lifetime': self.lowest_lifetime,
            'uncaught_count': self.uncaught_count,
            'percentage_caught': self.percentage_caught,
            'objects': objects,
            'active_objects': active_objects
        }

    def deactivate(self, target, caught=True):
        target.is_active = False
        target.caught = caught
        try:
            self._active_objects.remove(target)
        except ValueError:
            pass

    def delete(self, target):
        if target in self._objects:
            self._objects.remove(target)

        if target in self._active_objects:
            self._active_objects.remove(target)

    @property
    def average_lifetime(self):
        lifetimes = [obj.lifetime for obj in self.all]
        return sum(lifetimes) / len(lifetimes) if lifetimes else 0

    @property
    def highest_lifetime(self):
        return max(obj.lifetime for obj in self.all) if self.all else None

    @property
    def lowest_lifetime(self):
        return min(obj.lifetime for obj in self.all) if self.all else None

    @property
    def uncaught_count(self):
        return len([obj for obj in self.all if obj.caught is False])

    @property
    def percentage_caught(self):
        return (100 - self.uncaught_count / self.count * 100
                if self.count else 100)


class MovingTargetManager(TargetManager):
    model = MovingTarget
