"""
Environment which manages forces between map items.
"""
from network import fields
from network import pywans_random
from network.helpers import Force, sign


class Environment:
    K = fields.PositiveNumberField('ENVIRONMENT_K')
    map_size = fields.PositiveNumberField('ENVIRONMENT_MAP_SIZE')
    wall_load = fields.NumberField('ENVIRONMENT_WALL_LOAD')
    global_load = fields.BooleanField('ENVIRONMENT_GLOBAL_LOAD')

    def __init__(self, map_size=None, wall_load=None, k=None,
                 nodes=None, targets=None):
        if k is not None:
            self.K = k
        if map_size is not None:
            self.map_size = map_size
        if wall_load is not None:
            self.wall_load = wall_load

        self.nodes = nodes
        self.targets = targets

    @property
    def as_json(self):
        return {
            'K': self.K,
            'map_size': self.map_size,
            'wall_load': self.wall_load
        }

    def calculate_force(self, node):
        return Force.sum(
            self.get_walls_forces(node),
            self.get_nodes_forces(node)
        )

    def get_nodes_forces(self, node, exclude=True):
        if self.global_load is False:
            nodes = self.nodes.nodes_in_range(node.position, exclude=exclude)
        else:
            nodes = self.nodes.all.copy()

        forces = [
            self.get_node_force(node, item)
            for item in nodes if item is not node
        ]

        if exclude is False:  # pragma: no cover
            forces.append(self.get_node_force(node, node))

        return Force.sum(*forces)

    def get_node_force(self, node, item):
        distance_x = node.position.x - item.position.x
        distance_y = node.position.y - item.position.y

        force_x = sign(distance_x) * self.K * (node.load * item.load) / (distance_x ** 2) if distance_x != 0 else 0
        force_y = sign(distance_y) * self.K * (node.load * item.load) / (distance_y ** 2) if distance_y != 0 else 0

        # if 2 objects happened to be in 1 spot- move the away a little.
        if distance_x == 0 and distance_y == 0:
            force_x += pywans_random.uniform(-1, 1)
            force_y += pywans_random.uniform(-1, 1)

        force = Force(x=force_x, y=force_y)

        return force

    def get_walls_forces(self, obj):
        return Force.sum(
            self.get_bottom_force(obj),
            self.get_left_force(obj),
            self.get_top_force(obj),
            self.get_right_force(obj),
        )

    def get_top_force(self, obj):
        obj_x, obj_y = obj.position
        force_y = self.K * (obj.load * self.wall_load) / (obj_y ** 2)

        return Force(y=force_y)

    def get_bottom_force(self, obj):
        obj_x, obj_y = obj.position
        distance = self.map_size - obj_y
        force_y = -self.K * (obj.load * self.wall_load) / (distance ** 2) if distance else 0

        return Force(y=force_y)

    def get_left_force(self, obj):
        obj_x, obj_y = obj.position
        force_x = self.K * (obj.load * self.wall_load) / (obj_x ** 2)

        return Force(x=force_x)

    def get_right_force(self, obj):
        obj_x, obj_y = obj.position
        distance = self.map_size - obj_x
        force_x = -self.K * (obj.load * self.wall_load) / (distance ** 2) if distance else 0

        return Force(x=force_x)

    def get_target_by_position(self, position):
        target_generator = (
            target for target in self.targets.active
            if target.position == position
        )
        target = next(target_generator)

        return target
