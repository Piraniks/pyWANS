"""
Nodes positioned on map, which goal is to catch given objective (active part)
and passively position itself relatively to other nodes in an optimal pattern
(based on their loads and distance).
"""
from network.helpers import (Position, random_position, move_by_force,
                             get_distance, get_movement)
from network.statistics import MovementStatisticsMixin
from network import fields


class Node(MovementStatisticsMixin):
    """
    A map item which is considered subject of the study.
    """
    load = fields.NumberField('NODE_LOAD')
    speed = fields.PositiveNumberField('NODE_SPEED')
    sight_range = fields.PositiveNumberField('NODE_SIGHT_RANGE')

    def __init__(self, position=None, load=None, sight_range=None, speed=None,
                 is_active=True, name=None, target=None):
        super().__init__()

        if position is None:
            self.position = Position(*random_position())
        else:
            self.position = Position(*position)

        if load is not None:
            self.load = load
        if speed is not None:
            self.speed = speed
        if sight_range is not None:
            self.sight_range = sight_range

        self.is_active = is_active
        self.target = target
        self.name = name

    def __repr__(self):
        return (
            f'Node(load={self.load}, position={self.position}, '
            f'target={self.target.name if self.target else None})'
        )

    @property
    def as_json(self):
        return {
            'name': self.name,
            'load': self.load,
            'speed': self.speed,
            'sight_range': self.sight_range,
            'is_active': self.is_active,
            'target': self.target.name if self.target else None,
            'distance': self.distance_traveled
        }

    def distance(self, position):
        return get_distance(self, position)

    def move_by_force(self, force, map_size=100):
        movement = move_by_force(self, force, map_size=map_size)
        self.add_movement(movement)

    def is_in_range(self, position):
        node_y, node_x = self.position
        position_y, position_x = position
        return ((position_y - node_y) ** 2 + (position_x - node_x) ** 2) <= (self.sight_range ** 2)

    def move_to_position(self, position):
        """
        If target position is within sight of the object, it is moved to the
        location. Otherwise movement is calculated based on object's speed,
        position and direction of the movement.
        """
        distance = get_distance(self, position)
        movement = get_movement(self, position)
        if distance <= self.speed:
            self.position += movement
        else:
            self.position += get_movement(self, position)


class AttachedNode(Node):

    def __init__(self, *args, default_position=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.default_position = default_position
