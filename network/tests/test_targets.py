from unittest import mock

from network.targets import Target, MovingTarget


class TestTarget:
    def test_target_repr(self):
        target = Target()

        assert repr(target) == (
            f'Target(position={target.position}, lifetime={target.lifetime}, '
            f'caught={target.caught}, pursued_by={None})'
        )

    def test_target_str(self):
        target = Target()

        assert str(target) == (
            f'Target(position={target.position}, lifetime={target.lifetime}, '
            f'caught={target.caught}, pursued_by={None})'
        )

    def test_inc_life_time_of_target(self):
        target = Target()
        assert target.lifetime == 0

        target.age()
        assert target.lifetime == 1

        target.age(10)
        assert target.lifetime == 11

    def test_as_json_with_pursued_by_no_node(self):
        name = mock.MagicMock()
        lifetime = mock.MagicMock()
        max_lifetime = mock.MagicMock()
        pursued_by = None
        is_active = mock.MagicMock()
        caught = mock.MagicMock()

        target = Target(name=name, max_lifetime=max_lifetime,
                        pursued_by=pursued_by, is_active=is_active,
                        caught=caught)

        target.lifetime = lifetime

        assert target.as_json == {
            'name': name,
            'lifetime': lifetime,
            'max_lifetime': max_lifetime,
            'pursued_by': 'None',
            'is_active': is_active,
            'caught': caught
        }

    def test_as_json_with_pursued_by_a_node(self):
        name = mock.MagicMock()
        lifetime = mock.MagicMock()
        max_lifetime = mock.MagicMock()
        pursued_by = mock.MagicMock()
        is_active = mock.MagicMock()
        caught = mock.MagicMock()

        target = Target(name=name, max_lifetime=max_lifetime,
                        pursued_by=pursued_by, is_active=is_active,
                        caught=caught)

        target.lifetime = lifetime

        assert target.as_json == {
            'name': name,
            'lifetime': lifetime,
            'max_lifetime': max_lifetime,
            'pursued_by': pursued_by.name,
            'is_active': is_active,
            'caught': caught
        }


class TestMovingTarget:
    def test_target_repr(self):
        target = MovingTarget()

        assert repr(target) == (
            f'MovingTarget(position={target.position}, lifetime={target.lifetime}, '
            f'caught={target.caught}, pursued_by={target.pursued_by}, speed={target.speed})'
        )

    def test_target_str(self):
        target = MovingTarget()

        assert str(target) == (
            f'MovingTarget(position={target.position}, lifetime={target.lifetime}, '
            f'caught={target.caught}, pursued_by={target.pursued_by}, speed={target.speed})'
        )

    def test_inc_life_time_of_target(self):
        target = MovingTarget()
        assert target.lifetime == 0

        target.age()
        assert target.lifetime == 1

        target.age(10)
        assert target.lifetime == 11

    @mock.patch('network.targets.MovingTarget.move_to_position')
    def test_perform_actions(self, mmove_to_position):
        target = MovingTarget()
        msimulation = mock.MagicMock()

        sentinel = mock.sentinel
        msimulation.map_centre = sentinel

        target.perform_actions(simulation=msimulation)

        mmove_to_position.assert_called_once_with(sentinel)

    def test_move_to_position_in_range(self):
        start_position = (50, 50)
        aimed_position = (51, 51)
        end_position = aimed_position
        target = MovingTarget(speed=5, position=start_position)

        target.move_to_position(position=aimed_position)

        assert target.position == end_position

    def test_move_to_position_nout_of_range(self):
        start_position = (50, 50)
        aimed_position = (50, 60)
        end_position = (50, 55)
        target = MovingTarget(speed=5, position=start_position)

        target.move_to_position(position=aimed_position)

        assert target.position == end_position

    def test_as_json_with_pursued_by_no_node(self):
        name = mock.MagicMock()
        lifetime = mock.MagicMock()
        max_lifetime = mock.MagicMock()
        pursued_by = None
        is_active = mock.MagicMock()
        caught = mock.MagicMock()
        speed = mock.MagicMock()

        target = MovingTarget(name=name, max_lifetime=max_lifetime,
                              pursued_by=pursued_by, is_active=is_active,
                              caught=caught, speed=speed)

        target.lifetime = lifetime

        assert target.as_json == {
            'name': name,
            'lifetime': lifetime,
            'max_lifetime': max_lifetime,
            'pursued_by': 'None',
            'is_active': is_active,
            'caught': caught,
            'speed': speed
        }
