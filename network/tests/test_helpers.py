import random

import pytest

from network import pywans_random
from network.helpers import (
    Force, Position,
    sign, get_distance, get_movement,
    random_position, random_sidemap_position, move_by_force
)
from network.nodes import Node


class TestPosition:
    def test_create_position(self):
        x = 10
        y = 10
        position = Position(x=x, y=y)

        assert position.x == x
        assert position.y == y

    def test_position_repr(self):
        x = 10
        y = 10
        position = Position(x=x, y=y)

        assert repr(position) == f'Position(x={position.x}, y={position.y})'

    def test_position_str(self):
        x = 10
        y = 10
        position = Position(x=x, y=y)

        assert str(position) == f'Position(x={position.x}, y={position.y})'

    def test_position_equal_with_other_position(self):
        x = 10
        y = 10
        position = Position(x=x, y=y)
        other_position = Position(x=x, y=y)

        assert position == other_position

    def test_position_equal_with_position_tuple(self):
        x = 10
        y = 10
        position = Position(x=x, y=y)
        position_tuple = (x, y)

        assert position == position_tuple

    def test_position_add_to_other_position_object(self):
        x = 10
        y = 10

        position = Position(x=x, y=y)
        other_position = Position(x=x, y=y)

        position += other_position

        assert position == (x + x, y + y)

    def test_position_add_tuple_position(self):
        x = 10
        y = 10

        position = Position(x=x, y=y)
        other_position = (x, y)

        position += other_position

        assert position == (x + x, y + y)


class TestForce:
    def test_create_force(self):
        x = 10
        y = 10
        force = Force(x=x, y=y)

        assert force.x == x
        assert force.y == y

    def test_force_repr(self):
        x = 10
        y = 10
        force = Force(x=x, y=y)

        assert repr(force) == f'Force(x={force.x}, y={force.y})'

    def test_force_str(self):
        x = 10
        y = 10
        force = Force(x=x, y=y)

        assert str(force) == f'Force(x={force.x}, y={force.y})'

    def test_negate_force(self):
        x = 10
        y = 10

        force = Force(x=x, y=y)
        opposive_force = -force

        assert opposive_force.x == -x
        assert opposive_force.y == -y

    def test_add_forces(self):
        x1 = 10
        y1 = 10
        x2 = 20
        y2 = 20

        force1 = Force(x=x1, y=y1)
        force2 = Force(x=x2, y=y2)

        force3 = force1 + force2

        assert type(force3) == Force
        assert force3.x == force1.x + force2.x
        assert force3.y == force1.y + force2.y

    def test_subtract_forces(self):
        x1 = 20
        y1 = 20
        x2 = 10
        y2 = 10

        force1 = Force(x=x1, y=y1)
        force2 = Force(x=x2, y=y2)

        force3 = force1 - force2

        assert type(force3) == Force
        assert force3.x == force1.x - force2.x
        assert force3.y == force1.y - force2.y

    def test_sum_forces(self):
        x1 = 10
        y1 = 10
        x2 = 20
        y2 = 20

        force1 = Force(x=x1, y=y1)
        force2 = Force(x=x2, y=y2)

        force3 = Force.sum(force1, force2)

        assert type(force3) == Force
        assert force3.x == force1.x + force2.x
        assert force3.y == force1.y + force2.y


class TestGetDistance:
    @pytest.mark.parametrize('data', [
        [(50, 40), 10],
        [(45, 45), (5 ** 2 + 5 ** 2) ** (1 / 2)],
        [(50, 50), 0],
    ])
    def test_get_distance_with_different_position(self, data):
        position, expected_distance = data
        node = Node(position=(50, 50))

        distance = get_distance(node, position)

        assert expected_distance == distance


class TestSign:
    @pytest.mark.parametrize('data', [
        [-1, -1],
        [-1.0, -1],
        [-123456, -1],
        [1, 1],
        [1.0, 1],
        [123456, 1],
        [0, 0],
        [-0, 0],
    ])
    def test_sign_with_different_position(self, data):
        number, expected_sign = data

        assert expected_sign == sign(number)


class TestGetMovement:
    @pytest.mark.parametrize('data', [
        [(50, 50), (0, 0)],
        [(51, 51), (1, 1)],
        [(50, 55), (0, 5)],
        [(52, 54), (2, 4)],
        [(49, 49), (-1, -1)],
        [(50, 45), (0, -5)],
        [(48, 46), (-2, -4)],
        [(52, 48), (2, -2)],
        [(49, 53), (-1, 3)],
        [(100, 50), (6, 0)],
        [(50, 100), (0, 6)],
    ])
    def test_get_movement_with_the_same_positions(self, data):
        movement_position, expected_movement = data
        node = Node(position=(50, 50), speed=6, sight_range=20)

        movement = get_movement(node, movement_position)

        assert movement == expected_movement

    @pytest.mark.parametrize('data', [
        [(55, 55), (0.5, 0.5)],
        [(55, 45), (0.5, -0.5)],
        [(90, 90), (0.5, 0.5)],
        [(100, 50), (1, 0)],
        [(10, 50), (-1, 0)],
        [(51, 55), (1 / 6, 5 / 6)],
        [(49, 55), (-1 / 6, 5 / 6)],
        [(60, 55), (2 / 3, 1 / 3)],
        [(90, 60), (0.8, 0.2)],
        [(65, 55), (0.75, 0.25)],
        [(35, 55), (-0.75, 0.25)],
    ])
    def test_get_movement_with_big_distance_dimension_movement_is_equivalent_and_movement_sign(self, data):
        movement_position, expected_movement_percentage = data
        node = Node(position=(50, 50))

        movement = get_movement(node, movement_position)
        overall_movement = abs(movement.x) + abs(movement.y)

        movement_percentage = (
            movement.x / overall_movement,
            movement.y / overall_movement
        )

        assert pytest.approx(expected_movement_percentage) == movement_percentage


class TestMoveByForce:
    def test_no_movement_if_force_is_0(self):
        force = Force(0, 0)
        map_size = 100

        node_starting_position = (50, 50)
        node = Node(position=node_starting_position)

        move_by_force(obj=node, force=force, map_size=map_size)

        assert node.position == node_starting_position

    def test_no_movement_if_force_is_small_and_positive(self):
        force = Force(0.1, 0.1)
        map_size = 100

        node_starting_position = (50, 50)
        node = Node(position=node_starting_position)

        move_by_force(obj=node, force=force, map_size=map_size)

        assert node.position.x > Position(*node_starting_position).x
        assert node.position.y > Position(*node_starting_position).y

    def test_no_movement_if_force_is_small_and_negative(self):
        force = Force(-0.1, -0.1)
        map_size = 100

        node_starting_position = (50, 50)
        node = Node(position=node_starting_position)

        move_by_force(obj=node, force=force, map_size=map_size)

        assert node.position.x < Position(*node_starting_position).x
        assert node.position.y < Position(*node_starting_position).y

    def test_movement_by_force_does_not_change_position_object(self):
        force = Force(0, 0)
        map_size = 100

        node_starting_position = (50, 50)
        node = Node(position=node_starting_position)
        old_position = node.position

        move_by_force(obj=node, force=force, map_size=map_size)
        new_position = node.position

        assert new_position is old_position

    def test_more_movement_for_more_powerful_force(self):
        force_weak = Force(1, 1)
        force_strong = Force(1000, 1000)
        map_size = 100

        node_starting_position = (50, 50)
        node1 = Node(position=node_starting_position)
        node2 = Node(position=node_starting_position)

        move_by_force(obj=node1, force=force_weak, map_size=map_size)
        move_by_force(obj=node2, force=force_strong, map_size=map_size)

        assert node1.position.x < node2.position.x
        assert node1.position.y < node2.position.y

    def test_movement_does_not_exceed_map_bounds(self):
        force = Force(1000000, 1000000)
        map_size = 100

        node1 = Node(position=(1, 1))
        node2 = Node(position=(99, 99))

        move_by_force(obj=node1, force=-force, map_size=map_size)
        move_by_force(obj=node2, force=force, map_size=map_size)

        assert node1.position.x >= 1
        assert node1.position.y >= 1
        assert node2.position.x <= map_size - 1
        assert node2.position.y <= map_size - 1


class TestRandomPosition:
    def test_random_position_values_with_given_seed(self):
        # set seed for pywans_random position function
        seed = random.randint(0, 1000)
        pywans_random.seed(seed)

        position = random_position(10, 90)
        # restart seed to get the same values
        pywans_random.seed(seed)

        expected_position = (pywans_random.uniform(10, 90), pywans_random.uniform(10, 90))

        assert expected_position == position


class TestRandomSidemapPosition:
    # Test with seeds that cover all the possibilities instead of mocking.
    @pytest.mark.parametrize('seed', [
        1, 5, 7, 8
    ])
    def test_random_sitemap_position_with_valid_seed(self, seed):
        # set seed for pywans_random position function
        pywans_random.seed(seed)

        position = random_sidemap_position(10, 90)

        # restart seed to get the same values
        pywans_random.seed(seed)

        if pywans_random.randint(0, 1):
            if pywans_random.randint(0, 1):
                first = pywans_random.uniform(10, 11)
                second = pywans_random.uniform(10, 90)
            else:
                first = pywans_random.uniform(89, 90)
                second = pywans_random.uniform(10, 90)
        else:
            if pywans_random.randint(0, 1):
                first = pywans_random.uniform(10, 90)
                second = pywans_random.uniform(10, 11)
            else:
                first = pywans_random.uniform(10, 90)
                second = pywans_random.uniform(89, 90)

        expected_position = (first, second)

        assert expected_position == position

    # Test with seeds that cover all the possibilities instead of mocking.
    @pytest.mark.parametrize('seed', [
        1, 5, 7, 8
    ])
    def test_random_sitemap_position_with_seed_with_zero(self, seed):
        # set seed for pywans_random position function
        pywans_random.seed(seed)

        position = random_sidemap_position(0, 90)

        # restart seed to get the same values
        pywans_random.seed(seed)

        if pywans_random.randint(0, 1):
            if pywans_random.randint(0, 1):
                first = pywans_random.uniform(0, 1)
                second = pywans_random.uniform(0, 90)
            else:
                first = pywans_random.uniform(89, 90)
                second = pywans_random.uniform(0, 90)
        else:
            if pywans_random.randint(0, 1):
                first = pywans_random.uniform(0, 90)
                second = pywans_random.uniform(0, 1)
            else:
                first = pywans_random.uniform(0, 90)
                second = pywans_random.uniform(89, 90)

        expected_position = (first, second)

        assert expected_position == position
