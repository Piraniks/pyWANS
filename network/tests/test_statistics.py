from unittest import mock

from network.helpers import Movement
from network.statistics import MovementStatistics, MovementStatisticsMixin


class TestMovementStatistics:
    def test_movements_distance(self):
        movements = [Movement(first, second) for first in range(-5, 5) for second in range(-5, 5)]

        statistics = MovementStatistics()
        statistics.movements = movements

        expected_distance = sum(abs(movement.x) + abs(movement.y) for movement in movements)

        assert expected_distance == statistics.movements_distance


class TestMovementStatisticsMixin:
    @mock.patch('network.statistics.MovementStatistics.movements_distance')
    def test_distance_traveled(self, mmovements_distance):
        mixin = MovementStatisticsMixin()

        distance = mixin.distance_traveled

        assert distance == mmovements_distance
