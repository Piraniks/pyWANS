from unittest import mock

import pytest

from network.nodes import Node, AttachedNode


class TestNode:
    def test_node_repr(self):
        node = Node()

        assert repr(node) == (
            f'Node(load={node.load}, position={node.position}, '
            f'target={None})'
        )

    def test_node_str(self):
        node = Node()

        assert str(node) == (
            f'Node(load={node.load}, position={node.position}, '
            f'target={None})'
        )

    @pytest.mark.parametrize('data', [
        [(50, 40), 10],
        [(45, 45), (5 ** 2 + 5 ** 2) ** (1 / 2)],
        [(50, 50), 0],
    ])
    def test_distance_between_node_and_position(self, data):
        position, distance = data
        node = Node(position=(50, 50), sight_range=20)

        node_distance = node.distance(position)

        assert distance == node_distance

    @pytest.mark.parametrize('target', [
        None, mock.MagicMock()
    ])
    @mock.patch('network.nodes.Node.distance_traveled')
    def test_as_json_with_target_pursued(self, mdistance_traveled, target):
        name = mock.MagicMock()
        load = mock.MagicMock()
        speed = mock.MagicMock()
        sight_range = mock.MagicMock()
        is_active = mock.MagicMock()

        distance = mdistance_traveled

        node = Node(name=name, load=load, speed=speed, sight_range=sight_range,
                    is_active=is_active, target=target)

        assert node.as_json == {
            'name': name,
            'load': load,
            'speed': speed,
            'sight_range': sight_range,
            'is_active': is_active,
            'target': target.name if target is not None else None,
            'distance': distance,
        }


class TestAttachedNode:
    def test_create(self):
        default_position = mock.MagicMock()
        node = AttachedNode(default_position=default_position)

        assert node.default_position == default_position
