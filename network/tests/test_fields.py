from random import randint

import pytest

from network.fields import BaseField, PositiveFieldMixin, NotNegativeFieldMixin
import network_config


class TestBaseField:
    def test_init_with_valid_type(self):
        value = randint(0, 100)
        field_name = 'name'

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert field._value == value

    def test_init_with_invalid_type(self):
        value = 'invalid'
        field_name = 'name'

        setattr(network_config, field_name, value)

        with pytest.raises(ValueError):
            BaseField(field_name)

    def test_init_with_not_existing_config_value(self):
        field_name = 'not_existing_config_value'

        with pytest.raises(ValueError):
            BaseField(field_name)

    def test_gt(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field > compared_value) is (value > compared_value)

    def test_ge(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field >= compared_value) is (value >= compared_value)

    def test_lt(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field < compared_value) is (value < compared_value)

    def test_le(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field <= compared_value) is (value <= compared_value)

    def test_eq(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field == compared_value) is (value == compared_value)

    def test_ne(self):
        value = randint(0, 100)
        field_name = 'name'
        compared_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert (field != compared_value) is (value != compared_value)

    def test_add(self):
        value = randint(0, 100)
        field_name = 'name'
        added_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert field + added_value == value + added_value

    def test_radd(self):
        value = randint(0, 100)
        field_name = 'name'
        added_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert added_value + field == value + added_value

    def test_multiplication(self):
        value = randint(0, 100)
        field_name = 'name'
        multiplicated_value = randint(0, 100)

        setattr(network_config, field_name, value)

        BaseField.types = (int,)
        field = BaseField(field_name)

        assert field * multiplicated_value == value * multiplicated_value


class TestPositiveFieldMixin:
    def test_other_rules_with_positive_value(self):
        field = PositiveFieldMixin()
        positive_value = randint(1, 100)

        field.other_rules('name', positive_value)

    def test_other_rules_with_0(self):
        field = PositiveFieldMixin()

        with pytest.raises(ValueError):
            field.other_rules('name', 0)

    def test_other_rules_with_negative_value(self):
        field = PositiveFieldMixin()
        negative_value = randint(-100, -1)

        with pytest.raises(ValueError):
            field.other_rules('name', negative_value)


class TestNotNegativeFieldMixin:
    def test_other_rules_with_positive_value(self):
        field = NotNegativeFieldMixin()
        positive_value = randint(1, 100)

        field.other_rules('name', positive_value)

    def test_other_rules_with_0(self):
        field = NotNegativeFieldMixin()

        field.other_rules('name', 0)

    def test_other_rules_with_negative_value(self):
        field = NotNegativeFieldMixin()
        negative_value = randint(-100, -1)

        with pytest.raises(ValueError):
            field.other_rules('name', negative_value)
