import math
from collections import namedtuple
from random import randint
from unittest import mock

import pytest

from network.helpers import Position
from network.managers import TargetManager, NodeManager

MTarget = namedtuple('Target', 'lifetime, caught, as_json')
MNode = namedtuple('Node', 'distance_traveled, as_json, active')


class TestNodeManager:
    def test_creating_node(self):
        load = 30
        position_x = 10
        position_y = 20
        position = (position_x, position_y)

        nodes = NodeManager()

        node = nodes.create(load=load, position=position)

        assert node.position.x == position_x
        assert node.position.y == position_y
        assert node.load == load
        assert node.name == f'{node.__class__.__name__}1'
        assert node in nodes.active
        assert nodes.active_count == 1

    @pytest.mark.parametrize('data', [
        [(45, 45), True],
        [(10, 10), False],
        [(40, 50), True],
        [(math.ceil((50 - 5 * 2 ** (1 / 2)) * 1000) / 1000,
          math.ceil((50 - 5 * 2 ** (1 / 2)) * 1000) / 1000), True],
    ])
    def test_is_in_radius(self, data):
        position, in_radius = data
        nodes = NodeManager()

        node1 = nodes.create(position=(50, 50), sight_range=20, speed=6)
        node2 = nodes.create(position=position, sight_range=20, speed=6)

        assert node1.is_in_range(node2.position) == in_radius
        assert node2.is_in_range(node1.position) == in_radius

    def test_nodes_in_range(self):
        targets = TargetManager()
        nodes = NodeManager()

        target = targets.create(position=(45, 45))
        node = nodes.create(load=10, position=(50, 50), sight_range=10)
        node_in = nodes.create(load=10, position=(45, 45), sight_range=10)
        node_out = nodes.create(load=10, position=(40, 40), sight_range=10)

        nodes_in_range = nodes.nodes_in_range(node.position)

        assert target not in nodes_in_range
        assert node not in nodes_in_range
        assert node_in in nodes_in_range
        assert node_out not in nodes_in_range

    def test_get_closest_node_with_too_far_away_node(self):
        nodes = NodeManager()

        nodes.create(position=(10, 10))
        position = (50, 50)

        closest_node = nodes.closest(position=position)

        assert closest_node == []

    def test_get_closest_node_with_close_enough_node(self):
        nodes = NodeManager()

        node = nodes.create(position=(50, 50), sight_range=100)
        nodes.create(position=(80, 80), sight_range=100)
        nodes.create(position=(30, 30), sight_range=100)

        position = (60, 60)
        closest_node = nodes.closest(position=position)[0]

        assert closest_node is node

    def test_removing_node(self):
        nodes = NodeManager()
        node = nodes.create()

        assert node in nodes.active
        assert nodes.active_count == 1

        nodes.deactivate(node)

        assert nodes.active == []
        assert nodes.active_count == 0

    def test_get_node_active_objects_list(self):
        nodes = NodeManager()
        node1 = nodes.create()
        node2 = nodes.create()

        assert nodes.active == [node1, node2]

    def test_get_node_active_objects_counter(self):
        nodes = NodeManager()
        nodes.create()
        nodes.create()

        assert nodes.active_count == len(nodes.active) == 2

    def test_get_random_inactive_node(self):
        nodes = NodeManager()

        node = nodes.create(is_active=False)

        random_target = nodes.random(active=False)

        assert node not in nodes.active
        assert node.is_active is False
        assert node in nodes.all
        assert random_target is node

    def test_get_random_nodes_with_too_much_nodes(self):
        nodes = NodeManager()

        node = nodes.create()

        random_target = nodes.random(number=10)

        assert node in nodes.all
        assert random_target == [node]

    def test_deactivate_inactive_node(self):
        nodes = NodeManager()
        node = nodes.create(is_active=False)

        assert node not in nodes.active
        assert node in nodes.all

        nodes.deactivate(node)

        assert node not in nodes.active
        assert node in nodes.all

    def test_average_distance_traveled(self):
        nodes = NodeManager()

        node1 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())
        node2 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())

        nodes._active_objects = [node1, node2]
        distances = [node.distance_traveled for node in nodes.active]

        assert nodes.average_distance_traveled == sum(distances) / len(distances)

    def test_highest_distance_traveled(self):
        nodes = NodeManager()

        node1 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())
        node2 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())

        nodes._active_objects = [node1, node2]
        distances = [node.distance_traveled for node in nodes.active]

        assert nodes.highest_distance_traveled == max(distances)

    def test_lowest_distance_traveled(self):
        nodes = NodeManager()

        node1 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())
        node2 = MNode(distance_traveled=randint(0, 100), active=True, as_json=mock.MagicMock())

        nodes._active_objects = [node1, node2]
        distances = [node.distance_traveled for node in nodes.active]

        assert nodes.lowest_distance_traveled == min(distances)

    def test_as_json(self):
        nodes = NodeManager()

        for _ in range(randint(10, 50)):
            nodes.create()

        assert nodes.as_json == {
            'model': nodes.model.__name__,
            'active_count': nodes.active_count,
            'average_distance_traveled': nodes.average_distance_traveled,
            'lowest_distance_traveled': nodes.lowest_distance_traveled,
            'highest_distance_traveled': nodes.highest_distance_traveled,
            'count': nodes.count,
            'objects': [node.as_json for node in nodes._objects],
            'active_objects': [node.as_json for node in nodes._objects
                               if node.is_active is True],
        }


class TestTargetManager:
    def test_creating_target(self):
        position = (10, 10)
        targets = TargetManager()

        target = targets.create(position=position)

        assert target.position == Position(*position)
        assert target in targets.active
        assert targets.count == 1

    def test_removing_target(self):
        targets = TargetManager()

        target = targets.create()

        assert target in targets.active
        assert targets.count == 1
        assert targets.active_count == 1

        targets.deactivate(target)

        assert targets.active == []
        assert targets.count == 1
        assert targets.active_count == 0

    def test_get_target_active_objects_list(self):
        targets = TargetManager()

        target1 = targets.create()
        target2 = targets.create()

        assert targets.active == [target1, target2]

    def test_get_target_active_objects_counter(self):
        targets = TargetManager()

        targets.create()
        targets.create()

        assert targets.count == len(targets.active) == 2

    @pytest.mark.parametrize('caught', [
        True, False
    ])
    def test_deactivate_not_caught(self, caught):
        targets = TargetManager()

        target = targets.create()
        targets.deactivate(target, caught=caught)

        assert target not in targets.active
        assert target in targets.all
        assert target.caught is caught

    def test_deactivate_inactive_target(self):
        targets = TargetManager()
        target = targets.create(is_active=False)

        assert target not in targets.active
        assert target in targets.all

        targets.deactivate(target)

        assert target not in targets.active
        assert target in targets.all

    def test_average_lifetime(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())

        targets._objects = [target1, target2]
        lifetimes = [target.lifetime for target in targets.all]

        assert targets.average_lifetime == sum(lifetimes) / len(lifetimes)

    def test_highest_lifetime(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())

        targets._objects = [target1, target2]
        lifetimes = [target.lifetime for target in targets.all]

        assert targets.highest_lifetime == max(lifetimes)

    def test_lowest_lifetime(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())

        targets._objects = [target1, target2]
        lifetimes = [target.lifetime for target in targets.all]

        assert targets.lowest_lifetime == min(lifetimes)

    def test_uncaught_count(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target3 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target4 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target5 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())

        targets._objects = [target1, target2, target3, target4, target5]
        uncaught_targets = [target for target in targets.all if target.caught is False]

        assert targets.uncaught_count == len(uncaught_targets)

    def test_percentage_caught(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target3 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target4 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())
        target5 = MTarget(lifetime=randint(0, 100), caught=True, as_json=mock.MagicMock())

        targets._objects = [target1, target2, target3, target4, target5]

        assert targets.percentage_caught == 100 * 3 / targets.count

    def test_percentage_caught_with_no_caught(self):
        targets = TargetManager()

        target1 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target2 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target3 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target4 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())
        target5 = MTarget(lifetime=randint(0, 100), caught=False, as_json=mock.MagicMock())

        targets._objects = [target1, target2, target3, target4, target5]

        assert targets.percentage_caught == 0

    def test_as_json(self):
        targets = TargetManager()

        for _ in range(randint(10, 50)):
            targets.create()

        assert targets.as_json == {
            'model': 'Target',
            'active_count': len(targets.active),
            'count': len(targets.all),
            'average_lifetime': targets.average_lifetime,
            'highest_lifetime': targets.highest_lifetime,
            'lowest_lifetime': targets.lowest_lifetime,
            'uncaught_count': targets.uncaught_count,
            'percentage_caught': targets.percentage_caught,
            'objects': [target.as_json for target in targets._objects],
            'active_objects': [target.as_json for target in targets._objects
                               if target.is_active is True],
        }
