import json
import logging
import sys
from datetime import datetime
from math import sqrt

from network import fields
from network.environments import Environment
from network.helpers import (random_position, random_sidemap_position,
                             Position)
from network.managers import (NodeManager, AttachedNodeManager,
                              TargetManager, MovingTargetManager)

logger = logging.getLogger(__name__)
handler = logging.StreamHandler(sys.stdout)
logger.addHandler(handler)


class Simulation:
    """
    Simulations are being run as a part of the study.
    """
    environment = None
    ended = False
    environment_model = Environment
    node_manager = NodeManager
    target_manager = TargetManager

    node_number = fields.PositiveIntegerField('SIMULATION_NODE_NUMBER')
    target_number = fields.PositiveIntegerField('SIMULATION_TARGET_NUMBER')
    target_spawn_rate = fields.PositiveIntegerField('SIMULATION_TARGET_SPAWN_RATE')

    def __init__(self, nodes=None, targets=None, target_spawn_rate=None):
        # Nodes
        if nodes is not None:
            self.node_number = nodes

        # Targets
        if target_spawn_rate is not None:
            self.target_spawn_rate = target_spawn_rate
        if targets is not None:
            self.target_number = targets
        self.targets_to_spawn = self.target_number

        # Managers
        self.nodes = self.node_manager()
        self.targets = self.target_manager()

        # Environment
        self.create_environment(nodes=self.nodes, targets=self.targets)
        self.turn = 0
        self.map_centre = Position(self.map_size / 2, self.map_size / 2)

        # Statistics
        self.start_datetime = datetime.now()

    @property
    def as_json(self):
        json_data = {
            'model': type(self).__name__,
            'node_number': self.node_number,
            'target_spawn_rate': self.target_spawn_rate,
            'target_number': self.target_number,
            'targets_to_spawn': self.targets_to_spawn,
            'turn': self.turn,
            'environment': self.environment.as_json,
            'ended': self.ended
        }
        if self.ended:
            json_data.update(self.results)

        return json_data

    @property
    def results(self):
        return {
            'time_taken': str(datetime.now() - self.start_datetime),
            'nodes': self.nodes.as_json,
            'targets': self.targets.as_json
        }

    def create_environment(self, nodes, targets):
        self.environment = self.environment_model(nodes=nodes, targets=targets)

    def run(self, filename='results.txt'):  # pragma: no cover
        self.setup()

        while not self.ended:
            self.run_next_turn()
            if self.turn % 100 == 0:
                logger.info(f'Running turn: {self.turn}.')

        self.save_results(filename)
        logger.info(f'Simulation ran successfully! Results has been saved.')

    def run_next_turn(self, spawn=True):
        if spawn is True:
            self.spawn_target()

        self.nodes_pursue()
        self.move_nodes()
        self.make_actions_targets()
        self.age_targets()

        self.end_if_done()
        self.turn += 1

    def make_actions_targets(self):
        """
        If targets have actions to perform, perform them.
        """
        for target in self.targets.active:
            if target.has_actions:
                target.perform_actions(self)

    def nodes_pursue(self):
        """
        For every target that is not being pursued node if searched.
        If node if found- pursuit is established. If target is within
        reach of node, it is deactivated, node is moved and pursuit
        is ended. Node later on will be normally moved by loads.
        """
        for target in self.targets.active:
            # set pursuits for active targets if not set already
            if target.pursued_by is not None:
                continue

            # check all nodes in range, is any is found- stop searching
            for node in self.nodes.closest(target.position):
                if node.target is None:
                    target.pursued_by = node
                    node.target = target
                    break

    @property
    def map_size(self):
        return self.environment.map_size

    def move_nodes(self):
        """
        After nodes start pursuits, if they have a target they are moved
        towards the target. Otherwise they are moved by loads(forces).
        """
        nodes_with_forces = dict()
        for node in self.nodes.active:
            # calculate forces by loads if node has no target
            if node.target is None:
                force = self.environment.calculate_force(node)
                nodes_with_forces[node] = force

            # move targets towards target position if it is set
            else:
                target = node.target
                if target.is_active is False:
                    node.target = None
                    continue

                node.move_to_position(target.position)
                if node.distance(target.position) < node.speed:
                    self.targets.deactivate(target)
                    node.target = None

        # apply calculated forces
        for node, force in nodes_with_forces.items():
            node.move_by_force(force, map_size=self.map_size)

    def setup(self):
        for _ in range(self.node_number):
            self.nodes.create(position=random_position(1, self.map_size - 1))

    def spawn_target(self):
        """
        Spawns target if there are still targets to be spawned and if spawn
        rate aligns with current turn.
        """
        if self.turn % self.target_spawn_rate == 0 and self.targets_to_spawn > 0:
            target_position = random_position(1, self.map_size - 1)

            self.targets.create(position=target_position)
            self.targets_to_spawn -= 1

    def age_targets(self):
        for target in self.targets.active:
            target.age(1)
            if target.lifetime < target.max_lifetime:
                continue

            self.targets.deactivate(target, caught=False)

            # remove pursuer's target if given target is too old
            if target.pursued_by is not None:
                node = target.pursued_by
                target.pursued_by = None
                node.target = None

    def end_if_done(self):
        self.end_simulation_if_no_more_targets()

    def end_simulation_if_no_more_targets(self):
        if self.targets_to_spawn == 0 and self.targets.active_count == 0:
            self.ended = True

    def save_results(self, file_name='results.txt'):  # pragma: no cover
        with open(file_name, 'w+') as file:
            results = self.as_json

            file.write(json.dumps(results, indent=4))


class InvasionSimulation(Simulation):
    """
    Simulation in which targets spawn spawn on verges of the map
    and try to reach the centre. If the centre is reached, the
    target is considered not caught and deactivated.
    """
    target_manager = MovingTargetManager

    def age_targets(self):
        for target in self.targets.active:
            target.age(1)

    def spawn_target(self):
        if self.turn % self.target_spawn_rate == 0 and self.targets_to_spawn > 0:
            target_position = random_sidemap_position(1, self.map_size - 1)

            self.targets.create(position=target_position)
            self.targets_to_spawn -= 1

    def deactivate_if_in_centre(self):
        for target in self.targets.active:
            if target.position == self.map_centre:
                self.targets.deactivate(target, caught=False)

    def end_if_done(self):
        # Deactivate targets that reached the centre of the map.
        self.deactivate_if_in_centre()
        super().end_if_done()


class FailureSimulation(Simulation):

    def setup(self, number=None):
        super().setup()
        self.partial_failure(number=number)

    def partial_failure(self, number=None):
        if number is None:  # pragma: no cover
            number = fields.PositiveNumberField('SIMULATION_FAILED_NUMBER')

        if number < 1:
            failing_nodes = self.nodes.random(number=int(number * self.nodes.active_count),
                                              active=True)
        else:
            failing_nodes = self.nodes.random(number=int(number),
                                              active=True)

        # Call deactivate for all nodes
        [self.nodes.deactivate(node) for node in failing_nodes]


class FailureInvasionSimulation(FailureSimulation, InvasionSimulation):
    pass


class AttachedSimulation(Simulation):
    node_manager = AttachedNodeManager

    @property
    def position_generator(self):
        columns = int(sqrt(self.node_number))
        inc_columns = columns + 1
        size_per_column = self.map_size / inc_columns

        # Make the predefined positions for nodes
        for index_x in range(1, inc_columns):
            for index_y in range(1, inc_columns):
                coordinate_x = index_x * size_per_column
                coordinate_y = index_y * size_per_column
                new_position = Position(x=coordinate_x, y=coordinate_y)
                yield new_position

        # If more positions is needed- generate random ones.
        while True:
            yield random_position(1, self.map_size - 1)

    def setup(self):
        position_generator = self.position_generator
        # Create nodes.
        for index in range(self.node_number):
            chosen_position = next(position_generator)
            self.nodes.create(default_position=chosen_position,
                              position=chosen_position)

    def move_nodes(self):
        # If Node does not pursue a target- move it to the middle of it's field.
        for node in self.nodes.active:
            position_to_move = None
            # Move node towards target position if it is set.
            if node.target is not None:
                target = node.target
                if target.is_active is False:
                    # Target escaped.
                    node.target = None
                else:
                    position_to_move = target.position
                    if node.distance(position_to_move) < node.speed:
                        # Target is caught.
                        self.targets.deactivate(target)
                        node.target = None

            # Move node towards default position node has no target.
            if node.position != node.default_position and position_to_move is None:
                position_to_move = node.default_position

            node.move_to_position(position_to_move)


class FailureAttachedSimulation(FailureSimulation, AttachedSimulation):
    pass


class InvasionAttachedSimulation(InvasionSimulation, AttachedSimulation):
    pass


class FailureInvasionAttachedSimulation(FailureAttachedSimulation,
                                        InvasionAttachedSimulation):
    pass
