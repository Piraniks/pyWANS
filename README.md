# pyWANS

[![pipeline status](https://gitlab.com/Piraniks/pyWANS/badges/master/pipeline.svg)](https://gitlab.com/Piraniks/pyWANS/commits/master)
[![coverage report](https://gitlab.com/Piraniks/pyWANS/badges/master/coverage.svg)](https://gitlab.com/Piraniks/pyWANS/commits/master)

Python implemented Wireless (Sensor and) Actuator Network Simulator.

Complete environment to test multi-node network ideas.

## Run Project

Having installed Python 3.6 (on your OS or created and activated venv):

```
pip install Cython==0.28.5

# WINDOWS ONLY
pip install kivy.deps.glew==0.1.10
pip install kivy.deps.sdl2==0.1.18
# / WINDOWS ONLY

# LINUX ONLY
# May not be required.
pip install pygame
# / LINUX ONLY


pip install -r requirements.txt
```


After installing all dependencies to run project:

```
python run.py cli
```

You can also choose which simulation will be run and if logging to 
terminal will be turned on (default values below):

```
python run.py cli --simulation=Simulation --logger=True --repeats=1
```

Options for `simulation` are (case-sensitive!):
* Simulation
* FailureSimulation
* InvasionSimulation
* FailureInvasionSimulation

Options for `logger` are (case-insensitive):
* True
* everything else is treated as `False`

`repeats` is number of simulations of given class to run e.g. 
`--repeats=100` will run 100 simulations of given type and save results
to given files in `results` directory at the project's root.


You can also run GUI version by typing:

```
python run.py gui --simulation=Simulation
```

For the GUI logging is disabled, the simulation 
choices are the same as for the CLI version.



## Test

Before running tests, to install all requirements run:

```
pip install -r requirements-test.txt
```


To run tests:

```
py.test -v
```

